import Link from 'next/link';
//import styles from '../styles/Navbar.module.css';

const Navbar = () => {
  return (
    <nav className="uk-navbar-container uk-navbar" uk-navbar>
      <div className="uk-navbar-left">
      <Link className="uk-navbar-item uk-logo" href="/">Лого</Link>
      </div>
      <div className='uk-navbar-right uk-visible@s'>
      <ul className="uk-navbar-nav">
            <li>
            <Link href="/">Home</Link>
            </li>
            <li>
            <Link href="/shop">Shop</Link>
            </li>
            <li>
            <Link href="/cart">Cart</Link>
            </li>
        </ul>
      </div>
      <div className='uk-navbar-right uk-hidden@s'>
<ul className='uk-navbar-nav'>
  <li>
  <a className="uk-icon" uk-icon="icon: menu" href="#" aria-expanded="false"></a>
  </li>
</ul>
      </div>
    </nav>
  );
};

export default Navbar;