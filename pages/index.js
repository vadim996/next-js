import Head from 'next/head'
import Image from 'next/image'
import CategoryCard from '../components/CategoryCard';
import styles from '../styles/Home.module.css'

const HomePage = () => {
  return (
    <main className={styles.container}>
      <div className={styles.small}>
        <CategoryCard image="" name="Xbox" />
        <CategoryCard image="" name="PS5" />
        <CategoryCard image="" name="Switch" />
      </div>
      <div className={styles.large}>
        <CategoryCard image="" name="PC" />
        <CategoryCard
          image=""
          name="Accessories"
        />
      </div>
    </main>
  );
};

export default HomePage;